package ca.claurendeau.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.claurendeau.domaine.AnimalOwner;
import ca.claurendeau.domaine.Cat;
import ca.claurendeau.domaine.Dog;

public interface DogRepository extends JpaRepository<Dog, Long> {
    
    List<Dog> findBy(String animalName);

}
